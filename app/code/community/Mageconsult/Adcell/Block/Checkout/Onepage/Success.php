<?php

    /**
     * Class Mageconsult_Adcell_Block_Checkout_Onepage_Success
     */
    class Mageconsult_Adcell_Block_Checkout_Onepage_Success extends Mage_Core_Block_Template
    {

        /**
         * @var Mage_Sales_Model_Order
         */
        protected $_order = null;

        /**
         * Returns the order object
         *
         * @return Mage_Sales_Model_Order
         */
        protected function _getOrder()
        {
            if ($this->_order == null) {
                $this->_order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
            }
            return $this->_order;
        }

        /**
         * returns the ID of current sales order
         *
         * @return string
         */
        public function getOrderId()
        {
            return Mage::getSingleton('checkout/session')->getLastRealOrderId();
        }

        /**
         * returns order total excluding VAT
         * @return float
         */
        public function getOrderTotal()
        {
            $order = $this->_getOrder();

            $orderTotal = $order->getGrandTotal() - $order->getTaxAmount() - $order->getShippingAmount();

            return number_format($orderTotal,2,'.','');
        }

        /**
         * @param null $store
         * @return string
         */
        public function getEventId($store = null)
        {
            if (is_null($store)) {
                $store = Mage::app()->getStore()->getId();;
            }

            $trackingKey = Mage::getStoreConfig('mageconsult_adcell/tracking/event_id', $store);

            return $trackingKey;
        }

        /**
         * @param null $store
         * @return string
         */
        public function getProgramId($store = null)
        {
            if (is_null($store)) {
                $store = Mage::app()->getStore()->getId();;
            }

            $trackingKey = Mage::getStoreConfig('mageconsult_adcell/tracking/program_id', $store);

            return $trackingKey;
        }
    }