<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_Adcell extends Mage_Shell_Abstract
{


    /**
     * Run script
     *
     */
    public function run()
    {
        if ($this->getArg('create')) {

            Mage::app()->setCurrentStore(1);

            $baseUrl = 'http://www.stoffwerft.com/';

            $csv = array();

            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $productCollection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            $productCollection->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
            $productCollection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')));
            #$productCollection->addAttributeToSelect('*');

            echo $productCollection->getSize() . "\n";

            $header = array(
                'deeplink',
                'title',
                'short_description',
                'description',
                'price',
                'currency',
                'sku',
                'image_url',
                'thumbnail_url',
                'category',
                'shipping',
                'delivery-time',
                'basic-price',
                'basic-unit',
                );

            $csv[] = $header;

            /* @var $product Mage_Catalog_Model_Product */
            foreach ($productCollection as $product) {
                $product->load($product->getId());

                echo $product->getData('sku') . ";";

                // category
                $category = '';
                $categoryIds = $product->getCategoryIds();
                if(count($categoryIds) ){
                    $firstCategoryId = $categoryIds[0];
                    $category = Mage::getModel('catalog/category')->load($firstCategoryId);
                }

                // delivery date
                if ($product->isSalable()) {
                    $deliveryTime = $product->getData('delivery_time');
                }
                else {
                    $deliveryTime = 'zur Zeit nicht lieferbar';
                }

                // shipping costs
                $shipping = '3.9';
                if ($product->getAttributeSetId() == Stoffwerft_Custom_Helper_Data::ATTRIBUTE_SET_ID_NAEHSCHULE OR $product->getFinalPrice() >= 50) {
                    $shipping = '0.0';
                }


                $description = str_replace(array(';','"'),array(',',"'"), $product->getData('description'));
                $description = Zend_Filter::filterStatic($description, 'StripNewlines');

                $shortDescription = str_replace(array(';','"'),array(',',"'"), $product->getData('short_description'));
                $shortDescription = Zend_Filter::filterStatic($shortDescription, 'StripNewlines');

                $line = array(
                    'deeplink' => $baseUrl . $product->getUrlPath(),
                    'title' =>  str_replace(array(';','"'),array(',',"'"), $product->getData('name')),
                    'short_description' => $shortDescription,
                    'description' =>  $description,
                    'price' =>  $product->getFinalPrice(),
                    'currency' =>  'EUR',
                    'sku' =>  $product->getData('sku'),
                    'image_url' =>  (string) Mage::helper('catalog/image')->init($product, 'image')->resize(650),
                    'thumbnail_url' =>  (string) Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(65),
                    'category' =>  str_replace(';',',',$category->getName()),
                    'shipping' =>  $shipping,
                    'delivery_time' =>  $deliveryTime,
                    'basic-price' =>  $product->getFinalPrice(),
                    'basic-unit' =>  'Stck',
                );

                $csv[] = $line;
            }


            $fp = fopen(Mage::getBaseDir() . '/media/csv/adcell.csv', 'w');

            foreach ($csv as $fields) {
                #echo implode(';', $fields);

                fputcsv($fp, $fields, ';', '"');
            }
            fclose($fp);

        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f adcell.php -- [options]
        php -f adcell.php --create

  create            Create CSV
  help              This help

USAGE;
    }
}

$shell = new Mage_Shell_Adcell();
$shell->run();
